
"use strict";

let express = require("express");
let cookieParser = require("cookie-parser");
let bodyParser = require("body-parser");
let dotenv = require("dotenv");
let path = require("path");
let logger = require("morgan");
const log = require('./utils/logger');
let compression = require("compression");

// let requestId = function requestId (req, res, next) {
//   req.requestId = cuid();
//   next();
// };

// Then, at the top of your middleware:

let app = express();

/**
 * Load environment variables from .env file, where API keys and passwords are configured.
 */
if (app.get("env") === "dev" || app.get("env") === "development") {
  dotenv.load({ path: ".env" });
} else if (app.get("env") === "test") {
  dotenv.load({ path: ".env.example" });
}

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

app.set("json spaces", 4);
app.set("x-powered-by", false);
app.use(logger("dev"));
// gzip compression
app.use(compression());
// cache enabled
//cahce is disable now
app.set("view cache", false);


app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public"))); // serve static files before auth
// app.use(requestId);
/**
 * Auth middleware
 */



function logErrors (err, req, res, next) {
  log.error(err.stack);
  next(err);
}

function clientErrorHandler (err, req, res, next) {
  if (req.xhr) {
    res.render("500", { message: "Something failed!", error: "Something failed!" });
  } else {
    next(err);
  }
}

function errorHandler (err, req, res, next) {
  res.status(500);
  res.render("500", { message: "Something failed!", error: err });
}

app.use(logErrors);
app.use(clientErrorHandler);
app.use(errorHandler);

let routes = require("./routes/index");
app.use("/", routes);

app.locals._ = require("underscore");
app.locals.DOMAIN_URL = `http://${process.env.DOMAIN_URL}`;

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  let err = new Error("Not Found");
  err.status = 404;
  log.error(err);
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get("env") === "dev") {
  app.use(function (err, req, res, next) {
    log.error(err);
    res.status(err.status || 500);
    res.render("error", {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
  log.error(err);
  res.status(err.status || 500);
  res.render("error", {
    message: err.message,
    error: {}
  });
});

module.exports = app;

