module.exports = {
  LOGIN : "/login",
  DASHBOARD : "/dashboard",
  ALL_PAGES : "/all-pages",
  ALL_PRODUCTS : "/all-products",
  BLOG : "/blog",
  ADD_PRODUCT : "/add-product",
  PROFILE_SETTING : "/setting/profile",
  GET_ALL_PRODUCTS : "/license/product",
  PUT_PRODUCT : "/license/product/:product_slug",
  DELETE_PRODUCT : "/license/product/:product_slug",
  BOOKING_INFO :"/booking-info"



}