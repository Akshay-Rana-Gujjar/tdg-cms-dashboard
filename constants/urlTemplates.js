module.exports = {
  PRODUCT : {
    url : "/license/:domain/product/:product", method : "GET"
  },
  POST_PRODUCT : {
    url : "/license/product", method :"POST"
  },
  ALL_PRODUCT : {
    url : "/license/:domain", method : "GET"
  },
  POST_FLICKR : {
    url : "/license/flickr", method : "POST"
  },
  GET_PRODUCT : {
    url :"/license/:domain/product", method : "GET"
  },
  PRODUCT_IMAGE_POST : {
    url : "/license/product/image", method : "POST"
  },
  GET_BOOKING : {
    url :"/", method : "GET"
  },
  GET_TDG_PRODUCT : {
    url : "/plan", method : "GET"
  },
  POST_TDG_PRODUTC_TO_SITE : {
    url : "/license/product", method :"POST"
  },
  POST_PAGE : {
    url : "/license/page", method : "POST"
  },
  CURRENCY_CONVERTER : {
    url : "/license/currency/converter", method : "POST"
  }
}