/**
 * GET /*
 * Home page.
 */

"use strict";

let apiUtils = require("../utils/apiUtils");
let linkManager = require("../utils/linkManager");
let urlTemplates = require("../constants/urlTemplates");
let api_url_domain = process.env.API_URL;
let tdg_product_api = process.env.TDG_PRODUCT_API;
let loginCredentials = require("../constants/loginCredentials");

/**
 *
 * @type {{index: Function}}
 */
module.exports = {
  index: function (req, res) {

    let username = req.query.username;
    let password = req.query.password;

    if ((loginCredentials[username] && loginCredentials[username].password == password) || (req.cookies.domain && req.cookies.LID)) {
      if (!req.cookies.domain || !req.cookies.LID) {

        res.cookie('LID', loginCredentials[username].licensee);
        res.cookie('domain', loginCredentials[username].domain);

      }

      return res.render("dashboard");
    } else {

      return res.redirect("login/?wrongCredentials=true");
    }
  },
  login: function (req, res) {
    var passVariableForValidation = {
      "wrongCredentials": req.query.wrongCredentials
    }


    return res.render("login", passVariableForValidation);
  },
  all_pages: function (req, res) {
    return res.render("all_pages");
  },
  all_products: function (req, res) {
    return res.render("all_products");
  },
  add_product: function (req, res) {

    req.body.licensee = req.cookies.LID;
    let body = req.body;
    let base64Image = body.image;
    let media = body.media;
    delete body.image;
    delete body.media;

    let primaryImage = {
      title: "New Pic",
      description: "PLease don't use this image without permission",
      image_code: base64Image
    };

    var options = {
      url: linkManager.getApiUrlQuery(api_url_domain + urlTemplates.POST_FLICKR.url),
      method: urlTemplates.POST_FLICKR.method,
      body: primaryImage,
      json: true
    };

    // first primary image upload request
    apiUtils.httpRequest(options).then(function (primaryFlickrImageResponse) {

      if (primaryFlickrImageResponse.message == "Success") {
        body.image_url = primaryFlickrImageResponse.result.image_url;
        body.thumb_url = primaryFlickrImageResponse.result.thumb_url;


        var options = {
          url: linkManager.getApiUrlQuery(api_url_domain + urlTemplates.POST_PRODUCT.url),
          method: urlTemplates.POST_PRODUCT.method,
          body: body,
          json: true
        };

        // add product to db request
        apiUtils.httpRequest(options).then(function (productResponse) {

          // check have others images(carousel images) or not
          if (!media || !media.length || !media[0].image) {
            return res.send(productResponse);
          };

          let promiseList = [];

          for (var i = 0; i < media.length; i++) {

            let carouselImageToFlickr = {
              title: media[i].caption,
              description: "Please don't use this image without permission",
              image_code: media[i].image
            }

            var options = {
              url: linkManager.getApiUrlQuery(api_url_domain + urlTemplates.POST_FLICKR.url),
              method: urlTemplates.POST_FLICKR.method,
              body: carouselImageToFlickr,
              json: true
            }
            promiseList.push(apiUtils.httpRequest(options));
          }

          Promise.all(promiseList).then(function (flickrCarouselImageResponseArray) {

            let promiseList = [];

            for (var i = 0; i < media.length; i++) {

              let carouselImageToApi = {
                caption: media[i].caption,
                image_url: flickrCarouselImageResponseArray[i].result.image_url,
                thumb_url: flickrCarouselImageResponseArray[i].result.thumb_url,
                licensee: productResponse.result.product.licensee,
                licensee_product: productResponse.result.product.id
              }

              var options = {
                url: linkManager.getApiUrlQuery(api_url_domain + urlTemplates.PRODUCT_IMAGE_POST.url),
                method: urlTemplates.PRODUCT_IMAGE_POST.method,
                body: carouselImageToApi,
                json: true
              }
              promiseList.push(apiUtils.httpRequest(options));
            }

            Promise.all(promiseList).then(function (data) {
              res.send(data);
            }, function (err) {
              return res.status(500).send("Error");
            });

          }, function (err) {
            return res.status(500).send("Error");
          });
        }, function (err) {
          return res.status(500).send("Error");
        });
      }
    }, function (err) {
      return res.status(500).send("Error");
    });
  },

  profile_setting: function (req, res) {
    return res.render("profile_setting");
  },

  get_all_products: function (req, res) {

    var domainName = req.cookies.domain;


    var options = {
      url: linkManager.getApiUrlQuery(api_url_domain + "/license/" + domainName + "/product"),
      method: urlTemplates.GET_PRODUCT.method,
      json: true
    }

    apiUtils.httpRequest(options).then(function (data) {
      res.send(data);
    }, function (err) {
      return res.status(500).send("Error");
    });
  },

  blog: function (req, res) {
    return res.render("blog");
  },
  get_blog: function (req, res) {
    var domainName = req.cookies.domain;

    var options = {
      url: linkManager.getApiUrlQuery(api_url_domain + "/license/" + domainName + "/wordpress/blog"),
      method: "GET",
      json: true
    }

    apiUtils.httpRequest(options).then(function (data) {
      res.send(data);
    }, function (err) {
      return res.status(500).send("Error");
    });
  },

  update_product: function (req, res) {

    req.body.licensee = req.cookies.LID;
    let body = req.body;
    let base64Image = body.image;
    let media = body.media;
    delete body.image;
    delete body.media;
    delete body.images;

    if (body.image_url && !base64Image) {

      var options = {
        url: linkManager.getApiUrlQuery(api_url_domain + "/license/" + req.cookies.domain + "/product/" + req.params.product_slug),
        method: "PUT",
        body: body,
        json: true
      };

      // add product to db request
      apiUtils.httpRequest(options).then(function (productResponse) {

        // check have others images(carousel images) or not
        if (!media || !media.length || !media[0].image) {
          return res.send(productResponse);
        };

        let promiseList = [];

        for (var i = 0; i < media.length; i++) {

          let carouselImageToFlickr = {
            title: media[i].caption,
            description: "Please don't use this image without permission",
            image_code: media[i].image
          }

          var options = {
            url: linkManager.getApiUrlQuery(api_url_domain + urlTemplates.POST_FLICKR.url),
            method: urlTemplates.POST_FLICKR.method,
            body: carouselImageToFlickr,
            json: true
          }
          promiseList.push(apiUtils.httpRequest(options));
        }

        Promise.all(promiseList).then(function (flickrCarouselImageResponseArray) {

          let promiseList = [];

          for (var i = 0; i < media.length; i++) {

            let carouselImageToApi = {
              caption: media[i].caption,
              image_url: flickrCarouselImageResponseArray[i].result.image_url,
              thumb_url: flickrCarouselImageResponseArray[i].result.thumb_url,
              licensee: productResponse.result.product.licensee,
              licensee_product: productResponse.result.product.id
            }

            var options = {
              url: linkManager.getApiUrlQuery(api_url_domain + urlTemplates.PRODUCT_IMAGE_POST.url),
              method: urlTemplates.PRODUCT_IMAGE_POST.method,
              body: carouselImageToApi,
              json: true
            }
            promiseList.push(apiUtils.httpRequest(options));
          }

          Promise.all(promiseList).then(function (data) {
            res.send(data);
          }, function (err) {
            return res.status(500).send("Error");
          });

        }, function (err) {
          return res.status(500).send("Error");
        });
      }, function (err) {
        return res.status(500).send("Error");
      });
    } else {


      let primaryImage = {
        title: "New Pic",
        description: "PLease don't use this image without permission",
        image_code: base64Image
      };

      var options = {
        url: linkManager.getApiUrlQuery(api_url_domain + urlTemplates.POST_FLICKR.url),
        method: urlTemplates.POST_FLICKR.method,
        body: primaryImage,
        json: true
      };

      // first primary image upload request
      apiUtils.httpRequest(options).then(function (primaryFlickrImageResponse) {


        if (primaryFlickrImageResponse.message == "Success") {
          body.image_url = primaryFlickrImageResponse.result.image_url;
          body.thumb_url = primaryFlickrImageResponse.result.thumb_url


          var options = {
            url: linkManager.getApiUrlQuery(api_url_domain + "/license/" + req.cookies.domain + "/product/" + req.params.product_slug),
            method: "PUT",
            body: body,
            json: true
          }

          // update product to db request
          apiUtils.httpRequest(options).then(function (productResponse) {

            if (!media || !media.length || !media[0].image) {
              return res.send(productResponse);
            };

            let promiseList = [];

            for (var i = 0; i < media.length; i++) {

              let carouselImageToFlickr = {
                title: media[i].caption,
                description: "PLease don't use this image without permission",
                image_code: media[i].image
              }

              var options = {
                url: linkManager.getApiUrlQuery(api_url_domain + urlTemplates.POST_FLICKR.url),
                method: urlTemplates.POST_FLICKR.method,
                body: carouselImageToFlickr,
                json: true
              }
              promiseList.push(apiUtils.httpRequest(options));
            }

            Promise.all(promiseList).then(function (flickrCarouselImageResponseArray) {

              let promiseList = [];

              for (var i = 0; i < media.length; i++) {

                let carouselImageToApi = {
                  caption: media[i].caption,
                  image_url: flickrCarouselImageResponseArray[i].result.image_url,
                  thumb_url: flickrCarouselImageResponseArray[i].result.thumb_url,
                  licensee: productResponse.result.product.licensee,
                  licensee_product: productResponse.result.product.id
                }

                var options = {
                  url: linkManager.getApiUrlQuery(api_url_domain + urlTemplates.PRODUCT_IMAGE_POST.url),
                  method: urlTemplates.PRODUCT_IMAGE_POST.method,
                  body: carouselImageToApi,
                  json: true
                }
                promiseList.push(apiUtils.httpRequest(options));
              }

              Promise.all(promiseList).then(function (data) {
                res.send(data);
              }, function (err) {
                return res.status(500).send("Error");
              });

            }, function (err) {
              return res.status(500).send("Error");
            });
          }, function (err) {
            return res.status(500).send("Error");
          });
        }
      }, function (err) {
        return res.status(500).send("Error");
      });
    }

  },
  // delete product to db request
  delete_product: function (req, res) {
    var options = {
      url: linkManager.getApiUrlQuery(api_url_domain + "/license/" + req.cookies.domain + "/product/" + req.params.product_slug),
      method: "DELETE",
      json: true
    }
    apiUtils.httpRequest(options).then(function (data) {
      return res.send(data);
    }, function (err) {
      return res.status(500).send("Error");
    });

  },
  booking_info: function (req, res) {

    var options = {
      url: linkManager.getApiUrlQuery(api_url_domain + "/license/" + req.cookies.domain + "/user", req.query),
      method: "GET",
      json: true
    };

    apiUtils.httpRequest(options).then(function (booking_data) {
      res.send(booking_data);
    }, function (err) {
      return res.status(500).send("Error");
    });
  },
  booking: function (req, res) {
    return res.render("booking");
  },
  post_blog: function (req, res) {
    var body = req.body;
    body.post_status = "publish";
    body.post_tag = body.post_tag.split(",");
    body.category = body.category.split(",");

    var options = {
      url: linkManager.getApiUrlQuery(api_url_domain + "/license/" + req.cookies.domain + "/wordpress/blog"),
      method: "POST",
      body: body,
      json: true
    }

    apiUtils.httpRequest(options).then(function (data) {
      return res.send(data);
    }, function (err) {
      return res.status(500).send("Error");
    });
  },
  update_blog: function (req, res) {
    var body = req.body;
    var blog_id = body.id;
    var domain = req.cookies.domain;
    body.post_tag = body.post_tag.split(",");
    body.category = body.category.split(",");

    var options = {
      url: linkManager.getApiUrlQuery(api_url_domain + "/license/" + domain + "/wordpress/blog/" + blog_id),
      method: "PUT",
      body: body,
      json: true
    }
    apiUtils.httpRequest(options).then(function (data) {
      res.send(data);
    }, function (err) {
      return res.status(500).send("Error");
    });


  },
  new_product: function (req, res) {
    return res.render("new_product")
  },
  get_licensee: function (req, res) {

    var options = {
      url: linkManager.getApiUrlQuery(api_url_domain + "/license/" + req.cookies.domain),
      method: "GET",
      json: true
    }

    apiUtils.httpRequest(options).then(function (data) {
      res.send(data);
    }, function (err) {
      return res.status(500).send("Error");
    });
  },
  update_licensee: function (req, res) {
    var body = req.body
    var changeImage = body.changeImage;
    delete body.changeImage;

    if(!changeImage){
      var options = {
        url: linkManager.getApiUrlQuery(api_url_domain + "/license/" + req.cookies.domain),
        method: "PUT",
        json: true,
        body: body,
      };

      apiUtils.httpRequest(options).then(function (data) {
        res.send(data);
      }, function (err) {
        return res.status(500).send("Error");
      });

    }

    let flickrUploadImage = {
      title: "New Pic",
      description: "PLease don't use this image without permission",
      image_code: changeImage
    };

    var options = {
      url: linkManager.getApiUrlQuery(api_url_domain + urlTemplates.POST_FLICKR.url),
      method: urlTemplates.POST_FLICKR.method,
      body: flickrUploadImage,
      json: true
    };

    // first image upload request
    apiUtils.httpRequest(options).then(function (FlickrImageResponse) {

      if (FlickrImageResponse.message == "Success") {
        body.image_url = FlickrImageResponse.result.image_url;
        body.thumb_url = FlickrImageResponse.result.thumb_url;



        var options = {
          url: linkManager.getApiUrlQuery(api_url_domain + "/license/" + req.cookies.domain),
          method: "PUT",
          json: true,
          body: body,
        };

        apiUtils.httpRequest(options).then(function (data) {
          res.send(data);
        }, function (err) {
          return res.status(500).send("Error");
        });
      };
    });
  },
  get_tdg_products: function (req, res) {

    var queryParams = {
      "page": 1,
      "page_size": 10
    }

    var options = {
      url: linkManager.getApiUrlQuery(tdg_product_api + urlTemplates.GET_TDG_PRODUCT.url, queryParams),
      method: urlTemplates.GET_TDG_PRODUCT.method,
      json: true
    }

    apiUtils.httpRequest(options).then(function (data) {
      res.send(data);
    }, function (err) {
      return res.status(500).send("Error");
    });

  },

  add_tdg_product: function (req, res) {

    var products = req.body.products;
    var promiseList = [];
    var licensee_id = req.cookies.LID;

    for (var i = 0; i < products.length; i++) {
      var productData = {
        name: products[i].name,
        location: products[i].basecamp,
        duration: products[i].duration,
        base_price: products[i].markup_price,
        // offer_price : 
        travellers: products[i].travellers,
        presence: products[i].presence,
        pickup: products[i].pickup,
        // packs : 
        desc: products[i].desc,
        details: products[i].inclusion_text,
        // for(j in products[i].details){

        // }
        itinerary: products[i].itinerary,
        image_url: products[i].image_url,
        licensee: licensee_id,
        offer_price: products[i].offer_price
      };

      var options = {
        url: linkManager.getApiUrlQuery(api_url_domain + urlTemplates.POST_TDG_PRODUTC_TO_SITE.url),
        method: urlTemplates.POST_TDG_PRODUTC_TO_SITE.method,
        body: productData,
        json: true
      }

      promiseList.push(apiUtils.httpRequest(options));

    }

    Promise.all(promiseList).then((data) => {
      return res.send(data)
    }, (err) => {
      return res.status(500).send("Error");
    });

  },
  add_page: function (req, res) {

    var licensee_id = req.cookies.LID;
    var body = req.body;
    body.licensee = licensee_id;

    var options = {
      url: linkManager.getApiUrlQuery(api_url_domain + urlTemplates.POST_PAGE.url),
      method: urlTemplates.POST_PAGE.method,
      body: body,
      json: true
    };
    // add static page
    apiUtils.httpRequest(options).then(function (data) {
      return res.send(data);
    }, function (err) {
      return res.status(500).send("Error");
    })

  },
  get_pages: function (req, res) {
    var domainName = req.cookies.domain;


    var options = {
      url: linkManager.getApiUrlQuery(api_url_domain + "/license/" + domainName + "/page"),
      method: "GET",
      json: true
    }

    apiUtils.httpRequest(options).then(function (data) {
      res.send(data);
    }, function (err) {
      return res.status(500).send("Error");
    });

  },
  update_page: function (req, res) {
    var body = req.body;
    var page_id = body.id;

    var options = {
      url: linkManager.getApiUrlQuery(api_url_domain + "/license/page/" + page_id),
      method: "PUT",
      body: body,
      json: true
    }
    apiUtils.httpRequest(options).then(function (data) {
      res.send(data);
    }, function (err) {
      return res.status(500).send("Error");
    });
  },
  new_licensee: function (req, res) {

    return res.render("create_licensee");



  },
  create_licensee: function (req, res) {

    var body = req.body
    var changeImage = body.changeImage;
    delete body.changeImage;

    let flickrUploadImage = {
      title: "New Pic",
      description: "PLease don't use this image without permission",
      image_code: changeImage
    };

    var options = {
      url: linkManager.getApiUrlQuery(api_url_domain + urlTemplates.POST_FLICKR.url),
      method: urlTemplates.POST_FLICKR.method,
      body: flickrUploadImage,
      json: true
    };

    // first primary image upload request
    apiUtils.httpRequest(options).then(function (FlickrImageResponse) {

      if (FlickrImageResponse.message == "Success") {
        body.image_url = FlickrImageResponse.result.image_url;
        body.thumb_url = FlickrImageResponse.result.thumb_url;



        var options = {
          url: linkManager.getApiUrlQuery(api_url_domain + "/license/"),
          method: "POST",
          json: true,
          body: body,
        };
        apiUtils.httpRequest(options).then(function (data) {
          res.send(data);
        }, function (err) {
          return res.status(500).send("Error");
        });
      };
    }, function (err) {
      return res.status(500).send("Error");
    });

  },

  currency_converter : function(req, res){
    return res.render("currency_converter");

  },

  get_converted_currency : function(req, res){

    var body = req.body;

    var options = {
      url: linkManager.getApiUrlQuery(api_url_domain + urlTemplates.CURRENCY_CONVERTER.url),
      method: urlTemplates.CURRENCY_CONVERTER.method,
      json: true,
      body: body,
    };
    apiUtils.httpRequest(options).then(function (data) {
      return res.send(data);
    });


  }




};
