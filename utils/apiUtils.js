
"use strict";
let request = require("request");
let log = require("../helpers/logger");
let rp = require("request-promise");

/**
 * makes http call to server and also handles
 * @param options - configuration and data for call
 * @param cb - callback method to be executed when response is received
 */
module.exports.httpRequest = function (options) {
  
  return new Promise(function (resolve, reject) {
    // request(options, function (err, response, body) {
    //   if (err) {
    //     console.log(err);
    //     reject(err);
    //   } else {
    //     // console.log('Request time in ms', response.elapsedTime);
    //     log.info(`Request time in ${response.elapsedTime} ms`);
    //     resolve(body);
    //   }
    // });

    rp(options)
    .then(function (data) {
      // console.log(data);
      resolve(data);
    }).catch(function (err) {
      reject(err);
    });
  });
};

module.exports.turnIntoInstances = function (res) {
  // native Array.map ftw
  return res.map(function (rec) {
    return rec;
  });
};
