# Tdg-probe-Web 

### Version
1.0.0

### Tech

This dashboard uses some open source packages:

* [node.js] - evented I/O for the backend
* [Express] - fast node.js network app framework [@tjholowaychuk]
* [Grunt] - the streaming build system
* [Jade] - Templating engine to power html


### Installation

Dashboard requires [Node.js](https://nodejs.org/) v4.4+ to run.

You need grunt installed globally:

```sh
$ npm i -g grunt
```


```sh
$ git clone [git-repo-url] shell
$ cd shell
$ npm i -d
$ NODE_ENV=production node app
```

### Development

Want to contribute? Great!

Shell uses Grunt + Node for fast developing.
Make a change in your file and instantanously see your updates!

Open your favorite Terminal and run these commands.

First Tab:
```sh
$ node app
```

(optional) Second:
```sh
$ karma start
```


### Docker
Shell is very easy to install and deploy in a Docker container.

By default, the Docker will expose port 80, so change this within the Dockerfile if necessary. When ready, simply use the Dockerfile to build the image.

```sh
cd shell
docker build -t <youruser>/shell:latest .
```
This will create the shell image and pull in the necessary dependencies. Once done, run the Docker and map the port to whatever you wish on your host. In this example, we simply map port 80 of the host to port 80 of the Docker (or whatever port was exposed in the Dockerfile):

```sh
docker run -d -p 80:80 --restart="always" <youruser>/shell:latest
```

Verify the deployment by navigating to your server address in your preferred browser.

### NGINX

More details coming soon.

#### docker-compose.yml

Change the path for the nginx conf mounting path to your full path, not mine!

### Todos
 - Write Tests
 - Complete grunt setup for css, js and html 
 - Rethink Github Save
 - Add Code Comments
 - Write some code :)

**Awesome stuff, Hell Yeah!**


   [node.js]: <http://nodejs.org>
   [jQuery]: <http://jquery.com>
   [express]: <http://expressjs.com>
   [AngularJS]: <http://angularjs.org>
   [Grunt]: <http://gruntjs.com>
   [Jade]: <http://jade-lang.com/>

