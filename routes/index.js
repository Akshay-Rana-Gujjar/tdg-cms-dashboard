
"use strict";

let express = require("express");
let router = express.Router();
let routeUrls = require("../constants/routeUrls");
let dashboardController = require("../controllers/dashboardController");

router.get(routeUrls.DASHBOARD, dashboardController.index);
router.get(routeUrls.ALL_PAGES, dashboardController.all_pages);
router.get(routeUrls.ALL_PRODUCTS, dashboardController.all_products);
router.get(routeUrls.BLOG, dashboardController.blog);
router.get(routeUrls.PROFILE_SETTING, dashboardController.profile_setting);

router.get(routeUrls.LOGIN, dashboardController.login);
router.get(routeUrls.GET_ALL_PRODUCTS, dashboardController.get_all_products);
// TODO : 
// # separate controller from dashboard
// # convert routes from string to constants

router.get(routeUrls.BOOKING_INFO, dashboardController.booking_info)
router.get("/booking", dashboardController.booking);
router.get("/new-product", dashboardController.new_product)

router.get("/licensee", dashboardController.get_licensee);
router.put("/licensee", dashboardController.update_licensee);

router.post(routeUrls.ADD_PRODUCT, dashboardController.add_product);
router.put(routeUrls.PUT_PRODUCT, dashboardController.update_product);

router.post("/blog", dashboardController.post_blog);
router.put("/blog", dashboardController.update_blog);
router.get("/get-blog", dashboardController.get_blog);

router.delete(routeUrls.DELETE_PRODUCT, dashboardController.delete_product);

router.get("/tdg-products", dashboardController.get_tdg_products);
router.post("/add-tdg-product", dashboardController.add_tdg_product);

router.post("/page", dashboardController.add_page);
router.get("/page", dashboardController.get_pages);
router.put("/page", dashboardController.update_page);

router.get("/new", dashboardController.new_licensee);
router.post("/create-licensee", dashboardController.create_licensee);

router.get("/currency-converter", dashboardController.currency_converter);
router.post("/currency-converter", dashboardController.get_converted_currency);



module.exports = router;
