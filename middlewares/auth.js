"use strict";

let sessionUtils = require("../utils/sessionUtils");
let linkManager = require("../utils/linkManager");
let urlUtils = require("../utils/urlUtils");
// let authService = require("../services/authService");
let log = require("../helpers/logger");

module.exports = function (app) {
  app.use(function (req, res, next) {
    log.info("%s %s", req.method, req.url);
    if (sessionUtils.checkExists(req, res, "SID")) {
      log.info({req: req}, "SID exists");
      res.locals.SID = sessionUtils.getData(req, res, "SID");
    } else if (req.query["SID"]) {
      log.info({req: req}, "SID from param");
      sessionUtils.setData(req, res, "SID", req.query["SID"]);
      res.locals.SID = req.query["SID"];
    } else {
      log.info({req: req}, "SID not found");
      let uri = urlUtils.fullUrl(req);
      log.info({req: req}, uri);
      return res.redirect(linkManager.getApiUrlQuery(process.env.LOGIN_URL, {"redirect": uri}));
    }
    next();
  });
};