
module.exports = function (grunt) {

  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    clean: ['dist/'],
    compass: {
      dist: {
        options: {config: 'config.rb'},
      },
    },
    // Auto Prefixer
    autoprefixer: {
      dist: {
        options: {
          browsers: ['last 1 version', '> 1%', 'ie 8']
        },
        files: {
          'css/style-prefixed.css': ['css/style.css']
        }
      }
    },
    // Minify CSS
    cssmin: {
      combine: {
        files: {
          'css/style-min.css': ['css/style-prefixed.css']
        },
      },
    },
    // Minify JS
    uglify: {
      my_target: {
        files: {
          'js/scripts-min.js': ['js/src/theme.js']
        },
      },
    },
    // Watch
    watch: {
      compass: {
        files: 'assets/sass/**/*.scss',
        tasks: ['compass:dist'],
      },

      csspostprocess: {
        files: 'assets/css/style.css',
        tasks: ['autoprefixer', 'cssmin'],
      },

      jsminify: {
        files: 'assets/js/src/*.js',
        tasks: ['uglify'],
      },

      livereload: {
        options: {livereload: true},
        files: ['assets/css/*.css', 'assets/js/*.js', '*.html', 'assets/images/*'],
      },
    },
  });
  grunt.registerTask('default', ['watch'])
};