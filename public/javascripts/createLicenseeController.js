app.controller("createLicenseeController", ["$scope", "$http", function ($scope, $http) {

  $scope.licenseeInfo = {}


  $scope.saveProfile = function () {


    $http({
      url: api_urls.CREATE_LICENSEE,
      method: "POST",
      data: $scope.licenseeInfo
    }).then(function (res) {
      createToast("Saved!!", "green");
    }, function (res) {
      createToast("Something wents wrong!!", "red");
    })

  }



}])