app.controller("blogController", function ($scope, $http) {

  $("#summernote").summernote({
    height: 200
  });

  $scope.blog = {};
  // get all blog
  $http({
    url : api_urls.GET_BLOG,
    method : "GET"
  }).then(function(res){
    $scope.blogsInfo = res.data.result.blogs;
    

  },function(err){
    createToast("Something wents wrong!!", "red");
  });

  $scope.Publish = function () {

    var HTMLCode = $('#summernote').summernote('code');
    if (!HTMLCode) {
      return createToast("Please add content for your blog!!", "red");
    };

    createLoader("postLoader", "lightgreen")
    $scope.blog.content = HTMLCode;

    $scope.postApi("/blog", $scope.blog, "newBlogRes", function (res) {
      createToast("Blog Created!!", "green");
      removeLoader("postLoader");
    });
  };

  $scope.editBlog = function(blog){
    $scope.blog = {};
    $scope.blog = blog;
    $scope.blog.post_tag = blog.post_tag.join(", ");
    $scope.blog.category = blog.category.join(", ");
    $("#summernote").summernote("code", blog.content);
    $scope.function = "Update";
  }

  $scope.Update = function(){
    var HTMLCode = $('#summernote').summernote('code');
    if (!HTMLCode) {
      return createToast("Please add content for your blog!!", "red");
    };

    createLoader("postLoader", "lightgreen")

    $scope.blog.content = HTMLCode;

    $http({
      url : "/blog",
      method : "PUT",
      data : $scope.blog

    }).then(function(res){
      removeLoader("postLoader");// remove loader by id
      $('#addNewBlog').modal('hide');
      createToast("Blog Updated!!", "green");
    },function(err){
      removeLoader("postLoader");// remove loader by id
      createToast("Something went wrong!!", "red");
    });
  }

});