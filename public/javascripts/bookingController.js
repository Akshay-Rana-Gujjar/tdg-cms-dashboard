app.controller("bookingController", ["$scope", "$http",function($scope, $http){

  $scope.bookingInfo = [];
  $scope.queryInfo = [];

  queryParmaForBooking = "?is_booking=1";
  queryParmaForQuery = "?is_booking=0";

  $http({
    url : api_urls.BOOKING_INFO+queryParmaForBooking,
    method : "GET"
  }).then(function(res){
    $scope.bookingData = res.data.result.user;

  })

  $http({
    url : api_urls.BOOKING_INFO+queryParmaForQuery,
    method : "GET"
  }).then(function(res){
    $scope.queryData = res.data.result.user;

  })





}]);