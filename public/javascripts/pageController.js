app.controller("pageController", ["$scope", "$http", function($scope, $http){

  
  $scope.resetFields = function(){
    $scope.newPage = {};
    $('#summernote').summernote('code', "");
  }


  $("#summernote").summernote({
    height: 350
  });

  // get all pages
  $http({
    url : api_urls.PAGE,
    method : "GET"
  }).then(function(res){
    $scope.pagesInfo = res.data.result.page;
  },function(err){
    createToast("Something wents wrong!!", "red");
  });

  // add new static page
  $scope.Publish = function(){

    var HTMLCode = $('#summernote').summernote('code');
    if (!HTMLCode) {
      return createToast("Please add content for your blog!!", "red");
    };

    createLoader("PostApiLoader", "#ff4db9");// 1st agru :assign id to loader, 2nd agru : loader color

    $scope.newPage.content = HTMLCode;

    $http({
      url : api_urls.PAGE,
      method : "POST",
      data: $scope.newPage,
      json : true
    }).then(function(res){
      removeLoader("PostApiLoader");// 1st agru : remove loader by id
      $('#addNewPage').modal('hide');
      createToast("Page Published!!", "green");
      $scope.resetFields();

    },function(err){
      removeLoader("PostApiLoader");// 1st agru : remove loader by id
      createToast("Something wents wrong!!", "red");
    });
  }


  $scope.editPage = function(page){
    var pageData = page;
    $scope.newPage = pageData;
    $scope.function = "Update"
    $('#summernote').summernote('code', page.content);

  }

  $scope.Update = function(){
    var HTMLCode = $('#summernote').summernote('code');
    if (!HTMLCode) {
      return createToast("Please add content for your blog!!", "red");
    };

    createLoader("PostApiLoader", "#ff4db9");// 1st agru :assign id to loader, 2nd agru : loader color

    $scope.newPage.content = HTMLCode;
    

    $http({
      url : api_urls.PAGE,
      method : "PUT",
      data: $scope.newPage,
      json : true
    }).then(function(res){
      removeLoader("PostApiLoader");// 1st agru : remove loader by id
      $('#addNewPage').modal('hide');
      createToast("Page Edited!!", "green");
      $scope.resetFields();

    },function(err){
      removeLoader("PostApiLoader");// 1st agru : remove loader by id
      createToast("Something wents wrong!!", "red");
    });


  }

}]);