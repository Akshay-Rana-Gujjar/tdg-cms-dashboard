app.controller("productController", ["$scope", "$http", "upload", function($scope, $http, upload){
  
  $('#summernote').summernote({
    height : 180
  });
  $scope.resetFields = function(){
    $scope.newProduct = {};
    $scope.newProduct.presence = "1";
    $scope.newProduct.image = undefined;

    $scope.media = [{
      "image":"",
      "caption":""
    }];

    $scope.checkList = {
      products : []
    };
  };

  var productSlug, deleteElement;
  $scope.addMore  = function(){
    $scope.media.push({'image':'', 'caption':''});

  };
  var markupPercentage = 10;


  $scope.removeOne = function(){

      if($scope.media.length !=1 ){
          $scope.media.pop();
      }else{
        $scope.media = [{
          "image":"",
          "caption":""
        }];
      }
  };

  $http({
    url :  "/license/product",
    method : "GET"
  }).then(function(res){
    $scope.products = res.data.result.products;
  }, function(err){
      createToast("Something went worng!!", "red");
  });

  $scope.Add = function(){

    var HTMLCode = $('#summernote').summernote('code');

    if(!HTMLCode){
      return createToast("Itinerary filed is mandatory!!", "red");
    }
    
    if(!$scope.newProduct.image){
      return createToast("Please Select Primary Image!","red");
    }


    createLoader("PostApiLoader", "#ff4db9");// 1st agru :assign id to loader, 2nd agru : color
    
    $scope.newProduct.media = $scope.media;
    $scope.newProduct.itinerary = HTMLCode;

    $http({
      url : api_urls.ADD_PRODUCT,
      method : "POST",
      data : $scope.newProduct

    }).then(function(res){
      removeLoader("PostApiLoader");// remove loader by id
      $('#addNewProduct').modal('hide');
      createToast("Product Added!!", "green");
    },function(res){
      removeLoader("PostApiLoader");// remove loader by id
      createToast("Something went wrong!!", "red");
    });
  }

  $scope.Update = function(){


    if(!$scope.newProduct.image_url){
      if(!$scope.newProduct.image)
        return createToast("Please Select Primary Image!","red");
    }

    createLoader("PostApiLoader", "#ff4db9");// 1st agru :assign id to loader, 2nd agru : color

    $http({
      url : "/license/product/"+productSlug,
      method : "PUT",
      data : $scope.newProduct

    }).then(function(res){
      removeLoader("PostApiLoader");// remove loader by id
      $('#addNewProduct').modal('hide');
      createToast("Product Updated!!", "green");
    },function(res){
      removeLoader("PostApiLoader");// remove loader by id
      createToast("Something went wrong!!", "red");
    });
  }

  $scope.editProduct = function(product){
    $scope.function = 'Update';
    $scope.newProduct = product;
    $scope.newProduct.presence = product.presence.toString();
    productSlug = product.slug;
  };

  $scope.getProductSlug = function(product_slug, element){
    productSlug = product_slug;
    deleteElement = element.target;
  }

  $scope.deleteProduct = function(){

    $http({
      url : "/license/product/"+productSlug,
      method : "DELETE",

    }).then(function(res){
      $('#deleteProduct').modal('hide');
      $(deleteElement).parents("div.col-sm-6.col-md-3").remove();
      return createToast("Product Deleted!!", "green");
    })


  }

  

  $scope.showTdgProducts = function(){
    $scope.showTdgProductsContainer = true;

    createLoader("loadTdgProduct","green");

    $http({
      url :  "/tdg-products",
      method : "GET"
    }).then(function(res){
      removeLoader("loadTdgProduct");
      $scope.tdgProducts = res.data.result.plans;
    });

  };

  $scope.addTdgProductToSite = function(){

    createLoader("PostApiLoader", "#DA4453");// 1st agru :assign id to loader, 2nd agru : color

    $http({
      url : api_urls.ADD_TDG_PRODUCT,
      method : "POST",
      data : $scope.checkList

    }).then(function(res){
      removeLoader("PostApiLoader");// remove loader by id
      createToast("Product Added!!", "green");
      $scope.resetFields();
    },function(res){
      removeLoader("PostApiLoader");// remove loader by id
      createToast("Something went wrong!!", "red");
    });
  }


  // return price after adding markup in  tdg offer price
  $scope.tenPercentMarkupPrice = function(tdgPrice){

    var markUpValue = (tdgPrice * markupPercentage/100);
    var priceAfterMarkup = markUpValue + tdgPrice;

    return parseInt(priceAfterMarkup);

  }

  $scope.resetFields();
}]);