app.controller("dashboardController", ["$scope", "$http", function($scope, $http){

  // TODO : signout should be from server side
  $scope.signout = function(){
    delete_cookie("LID");
    window.location.href = "/login";
  }

  var delete_cookie = function(name) {
    document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
  }; 
  $scope.getApi = function(url, params, scopeVariable, callback){
    $http({
      url: url,
      method : "GET",
      params : params
    }).then(function(res){
      $scope[scopeVariable] = res.data;
      if(typeof callback == 'function'){
        callback(res);
      }
    });
  }

  $scope.postApi = function(url, data, scopeVariable, callback){
    $http({
      url: url,
      method : "POST",
      data : data
    }).then(function(res){
      $scope[scopeVariable] = res.data;
      if(typeof callback == 'function'){
        callback(res);
      }
    }, function(err){
      createToast("Something went wrong!!","red");
    });
  }
}]);