function createLoader(loaderId , loaderColor) {
  var loader = document.createElement("div");
  document.body.appendChild(loader);
  loader.id = loaderId || "loader";
  loader.className = "loader"
  loader.style.borderTopColor = loaderColor || "#3498db";

};

function removeLoader(loaderId) {
  var loader_id = loaderId || "loader";
  var loader = document.getElementById(loader_id);
  loader.remove();
};