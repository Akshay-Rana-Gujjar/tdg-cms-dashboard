app.controller("profileController" , ["$scope", "$http", "$filter", function($scope, $http, $filter){

  $scope.disable = {
    domain : true,
    licensee_number : true
  }

  $http({
    url : "/licensee",
    method : "GET"
  }).then(function(res){
    $scope.licenseeInfo = res.data.result.licensee;

    $scope.established_date = $filter('date')($scope.licenseeInfo.established_on , 'yyyy-MM-dd');
    
  },function(res){
    createToast("Something wents wrong!!", "red");
  });
  

  $scope.saveProfile = function(){

  $http({
      url : "/licensee",
      method : "PUT",
      data : $scope.licenseeInfo
    }).then(function(res){
     createToast("Saved!!", "green");
    },function(res){
      createToast("Something wents wrong!!", "red");
    })

  }
}]);