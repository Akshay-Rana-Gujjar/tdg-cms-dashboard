/**
 * Created by Himanshu wolf on 28/06/17.
 */


app = angular.module('dashboard', ['checklist-model','angular.filter', 'ngRoute', "lr.upload"]);

api_urls = {
  ALL_PAGES : "/all-pages",
  ALL_PRODUCTS : "/all-products",
  PROFILE_SETTING : "/setting/profile",
  ADD_PRODUCT : "/add-product",
  BLOG : "/blog",
  GET_BLOG : "/get-blog",
  BOOKING : "/booking",
  BOOKING_INFO : "/booking-info",
  ADD_TDG_PRODUCT : "/add-tdg-product",
  PAGE : "/page",
  CREATE_LICENSEE : "/create-licensee",
  CURRENCY_CONVERTER  : "/currency-converter"
  

};

route_controller = {
  PAGE : "pageController",
  PRODUCT : "productController",
  PROFILE_SETTING : "profileController",
  BLOG : "blogController",
  BOOKING : "bookingController",
  CURRENCY_CONVERTER : "currencyController"
}

app.directive("fileread", [function () {
  return {
      scope: {
          fileread: "="
      },
      link: function (scope, element, attributes) {
          element.bind("change", function (changeEvent) {
              var reader = new FileReader();
              reader.onload = function (loadEvent) {
                  scope.$apply(function () {
                      scope.fileread = loadEvent.target.result;
                  });
              }
              reader.readAsDataURL(changeEvent.target.files[0]);
          });
      }
  }
}]);

app.config(['$locationProvider', function($locationProvider) {
  $locationProvider.hashPrefix('');
}]);

app.config(function($routeProvider) {
  $routeProvider
  .when("/products",{
    templateUrl : api_urls.ALL_PRODUCTS,
    controller : route_controller.PRODUCT
  })
  .when("/home",{
    templateUrl : api_urls.BOOKING,
    controller : route_controller.BOOKING
  })
  .when("/pages", {
    templateUrl : api_urls.ALL_PAGES,
    controller : route_controller.PAGE
  })
  .when("/setting/profile", {
    templateUrl : api_urls.PROFILE_SETTING,
    controller : route_controller.PROFILE_SETTING
  })
  .when("/blog", {
    templateUrl : api_urls.BLOG,
    controller : route_controller.BLOG
  })
  .when("/currency-converter", {
    templateUrl : api_urls.CURRENCY_CONVERTER,
    controller : route_controller.CURRENCY_CONVERTER
  })
  
});

/**
 * url config for traveld'globe admin panel
 */
base_url = "/api/";


slug_url = {
  "location": "/location"
};

function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') c = c.substring(1);
    if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
  }
  return "";
}

var toggleOverlay = function() {
  var $overlay=  $('.fixed-overlay'), $body = $('body');
  if($overlay.hasClass('hide')) {
    $overlay.removeClass('hide');
    //$body.addClass('noscroll');
  } else {
    $overlay.addClass('hide');
    //$body.removeClass('noscroll');
  }
}

function getCookie(name) {
  var value = "; " + document.cookie;
  var parts = value.split("; " + name + "=");
  if (parts.length == 2) return parts.pop().split(";").shift();
}

var regex = {
  URL_WITH_PROTOCOL : /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g
}
