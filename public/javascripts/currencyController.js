app.controller("currencyController", ["$scope", "$http", function($scope, $http){

  $scope.currencies = ["usd", "inr"];
  $scope.convert = {};

  $scope.convertCurrency = function(){

    $http({
      url : api_urls.CURRENCY_CONVERTER,
      method : "POST",
      data : $scope.convert
    }).then(function(res){
      $scope.convertedCurrency = res.data.result;

    },function(err){
      createToast("Something went wrong!","red")
    });

  }
}]);